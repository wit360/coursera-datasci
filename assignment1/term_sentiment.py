import sys,json

def parseSentiment():
    sentiments = {}
    with open(sys.argv[1]) as f:
        for line in f.readlines():
            (word, score) = line.strip().split('\t')
            sentiments[word] = int(score)
    return sentiments

def parseTweet():
    tweets = []
    with open(sys.argv[2]) as f:
        for line in f.readlines():
            tweet = json.loads(line)
            if u'text' in tweet:
                tweets.append(tweet[u'text'])
            else:
                tweets.append('')

    return tweets

def main():
    sentiments = parseSentiment()
    tweets = parseTweet()
    scores = []
    newWords = {}
    
    for tweet in tweets:
        score = 0
        words = tweet.split()
        for word in words:
            if word not in sentiments: continue
            score += sentiments[word]
        for word in words:
            if word in sentiments: continue
            if word not in newWords: newWords[word] = (0,0)
            (sumSentiment,count) = newWords[word]
            newWords[word] = (sumSentiment+score,count+1)

    for word in newWords:
        (sumSentiment,count) = newWords[word]
        print "%s\t%f" % (word.encode('utf-8'),sumSentiment/float(count))


if __name__ == '__main__':
    main()
