import sys,json,collections,heapq

def parseHashtags():
    hashtagCounts = collections.Counter() 
    with open(sys.argv[1]) as f:
        for line in f.readlines():
            tweet = json.loads(line)
            if u'entities' not in tweet: continue
            if u'hashtags' in tweet[u'entities']:
                for hashtag in tweet[u'entities'][u'hashtags']:
                    hashtagCounts[hashtag[u'text']] += 1
    return hashtagCounts

def main():
    hashtagCounts = parseHashtags()
    toptens = heapq.nlargest(10, hashtagCounts, key=hashtagCounts.get)
    for hashtag in toptens:
        print hashtag, hashtagCounts[hashtag]

if __name__ == '__main__':
    main()
