import sys,json

def parseSentiment():
    sentiments = {}
    with open(sys.argv[1]) as f:
        for line in f.readlines():
            (word, score) = line.strip().split('\t')
            sentiments[word] = int(score)
    return sentiments

def parseTweet():
    tweets = []
    with open(sys.argv[2]) as f:
        for line in f.readlines():
            tweet = json.loads(line)
            if u'text' in tweet:
                tweets.append(tweet[u'text'])
            else:
                tweets.append('')

    return tweets

def main():
    sentiments = parseSentiment()
    tweets = parseTweet()

    for tweet in tweets:
        score = 0
        for word in tweet.split():
            if word not in sentiments: continue
            score += sentiments[word]
        print score


if __name__ == '__main__':
    main()
