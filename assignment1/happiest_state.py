import sys,json

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

fullStates = {} # for reverse lookup
for state in states: fullStates[states[state]] = state

def parseSentiment():
    sentiments = {}
    with open(sys.argv[1]) as f:
        for line in f.readlines():
            (word, score) = line.strip().split('\t')
            sentiments[word] = int(score)
    return sentiments

def parseTweet():
    tweets = []
    with open(sys.argv[2]) as f:
        for line in f.readlines():
            tweet = json.loads(line)
            tweets.append(tweet)
    return tweets

def getStateFromPlace(tweet):
    if u'place' not in tweet or tweet[u'place'] is None: return False
    if u'country_code' not in tweet[u'place']: return False
    if tweet[u'place'][u'country_code'] != 'US': return False
    if u'full_name' not in tweet[u'place']: return False

    tokens = tweet[u'place'][u'full_name'].split(', ')
    if tokens[-1] in states.keys(): return tokens[-1]
    if tokens[0] in fullStates.keys(): return fullStates[tokens[0]]
    return False

def getStateFromUser(tweet):
    if u'user' not in tweet: return False
    if u'location' not in tweet[u'user'] or tweet[u'user'][u'location'] is None: return False

    tokens = tweet[u'user'][u'location'].split(', ')
    if tokens[-1] in states.keys(): return tokens[-1]
    return False

def main():
    sentiments = parseSentiment()
    tweets = parseTweet()
    stateScore = {}
    for state in states: stateScore[state] = (0,0)
    
    for tweet in tweets:
        if u'text' not in tweet: continue
        state = getStateFromPlace(tweet)
        if state == False: state = getStateFromUser(tweet)
        if state == False: continue
        score = 0
        words = tweet[u'text'].split()

        for word in words:
            if word not in sentiments: continue
            score += sentiments[word]

        (sumSentiment,count) = stateScore[state]
        stateScore[state] = (sumSentiment+score,count+1)

    """
    for state in stateScore:
        (sumSentiment,count) = stateScore[state]
        print state,sumSentiment/float(max(count,1))
    """

    maxState = max(stateScore, key=lambda x: stateScore[x][0]/max(stateScore[x][1],1))
    print maxState

if __name__ == '__main__':
    main()
