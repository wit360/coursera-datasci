import sys,json

def parseTweet():
    tweets = []
    with open(sys.argv[1]) as f:
        for line in f.readlines():
            tweet = json.loads(line)
            if u'text' in tweet:
                tweets.append(tweet[u'text'])
            else:
                tweets.append('')

    return tweets

def main():
    tweets = parseTweet()
    wordCount = 0
    histogram = {}

    for tweet in tweets:
        for word in tweet.split():
            wordCount += 1
            if word not in histogram:
                histogram[word] = 0
            histogram[word] += 1

    for word in histogram:
        histogram[word] = float(histogram[word])/wordCount

    for word in histogram:
        print word,histogram[word]

if __name__ == '__main__':
    main()
