import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
	record = sorted(record)
	mr.emit_intermediate((record[0],record[1]),1)

def reducer(key, list_of_values):
	if len(list_of_values) == 1:
		mr.emit((key[0],key[1]))
		mr.emit((key[1],key[0]))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
