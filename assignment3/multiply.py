import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

'''
1 2 3     1 4 
4 5 6     2 5
				  3 6
'''

def mapper(record):
	  #A(LxM) B(MxN)
		L = 5
		N = 5

		matrix, i, j, value = record
		if matrix == 'a':
			for k in range(L):
				mr.emit_intermediate((i,k),(j,value))
		elif matrix == 'b':
			for k in range(N):
				mr.emit_intermediate((k,j),(i,value))

def reducer(key, list_of_values):
		i,j = key
		tmp = {}
		value = 0
		for k,v in list_of_values:
			if k in tmp: value+= v*tmp[k]
			else: tmp[k] = v
		mr.emit((i,j,value))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
